# ng-dashboard

Running 
```
npm start
```
For devs: in a separate terminal window run
```
gulp watch
```
### Technology Stack

* EcmaScript6 
* Angular 1.x
* JSPM 
* Gulp 

### Requirements
-------
* nodejs 4.+
* npm 3.+


### Gulp tasks
-------
Serve application to browser

```
gulp serve            # Serves application, watch *.js, reload

```

Compiling options

```
gulp compile:styles       # compile and move LESS files
gulp compile:views        # moves all templates to the serving folder
gulp compile:scripts      # compile all es6 files 
gulp watch                # compile and watch es6/views/styles files
```