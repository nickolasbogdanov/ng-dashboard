const gulp = require('gulp');

const path = require('path');

const paths = {
    app: ['./src/**/*.js'],
    dist: './dist/',
    partials: `${__dirname}/src/app/**/*.html`,
    mocks: `${__dirname}/src/app/mock/**/*.json`,
    index: ['index.html'],
    stylesSrc: './src/app/app.less',
    root: __dirname
};

const serve = require('./tasks/server')(paths.root, true);
const scripts = require('./tasks/babel')(gulp, paths.app, paths.dist);
const styles = require('./tasks/styles')(gulp, paths.stylesSrc, paths.dist + 'app');
const views = require('./tasks/views')(gulp, paths.partials, paths.dist + 'app');
const mocks = require('./tasks/mocks')(gulp, paths.mocks, paths.dist + 'app/mock');

gulp.task('serve', ['compile'], serve);
gulp.task('compile', ['compile:scripts', 'compile:styles', 'compile:views', 'mocks']);
gulp.task('compile:scripts', scripts);
gulp.task('compile:styles', [], styles);
gulp.task('compile:views', [], views);

gulp.task('mocks', [], mocks);

gulp.task('default', ['serve']);

gulp.task('watch', ['compile'], function () {
    gulp.watch('./src/app/**/*.js', ['compile:scripts']);
    gulp.watch('./src/app/mock/**/*.json', ['mocks']);
    gulp.watch('./src/app/**/*.html', ['compile:views']);
    gulp.watch('./src/**/*.less', ['compile:styles']);
});