import angular from 'angular';

import './core/core.module';
import './dashboard/dashboard.module';
import './layout/layout.module';
import './widgets/widgets.module';

angular.module('app', [
    'app.core',
    'app.dashboard',
    'app.layout',
    'app.widgets'
]);
