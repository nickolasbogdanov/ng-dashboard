import angular from 'angular';
import 'angular-ui-router';
import {configFunc} from './core.route';

const module = angular.module('app.core', ['ui.router']);

module.config(configFunc);