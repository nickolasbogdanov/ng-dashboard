// @ngInject
export function configFunc($urlRouterProvider) {
    $urlRouterProvider.otherwise('/dashboard');
}