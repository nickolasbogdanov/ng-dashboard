import angular from 'angular';
import {OverviewComponent} from './overview/overview.component';
import {configFunc} from './dashboard.route';
import {OverviewService} from './overview/overview.service';

const module = angular.module('app.dashboard', []);

module.component('overview', new OverviewComponent());
module.service('overviewService', OverviewService);

module.config(configFunc);