// @ngInject
export function configFunc($stateProvider) {
    $stateProvider.state('dashboard', {
        url: '/dashboard',
        template: '<overview></overview>'
    });
}