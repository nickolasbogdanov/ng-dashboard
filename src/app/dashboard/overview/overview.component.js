import {OverviewController} from './overview.controller';

export class OverviewComponent {
    constructor() {
        this.bindings = {
        };
        this.controller = OverviewController;
        this.controllerAs = 'vm';
        this.templateUrl = './dist/app/dashboard/overview/overview.partial.html';
    }
}