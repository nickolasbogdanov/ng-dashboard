export class OverviewController {
    // @ngInject
    constructor(overviewService) {
        this.overviewService = overviewService;
    }

    $onInit() {
        this.getCards();
        this.getTable();
    }

    getCards() {
        this.overviewService.getStatistics()
            .then((res) => this.cards = res.data)
            .catch((err) => console.log(`Error${err}`));
    }

    getTable() {
        this.overviewService.getTableData()
            .then((res) => this.tableData = res.data)
            .catch((err) => console.log(`Error${err}`));
    }


}