export class OverviewService {
    // @ngInject
    constructor($http){
        this.$http = $http;
    }

    getStatistics() {
        return this.$http.get('./dist/app/mock/statistics.json');
    }
    
    getTableData() {
        return this.$http.get('./dist/app/mock/table.data.json');
    }
}