import angular from 'angular';

import './app.module';

import './app.css!';

angular.element(document).ready(() => {
    angular.bootstrap(document, [
        'app'
    ], { strictDi: true });
});