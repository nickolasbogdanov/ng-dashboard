import {HeaderController} from './header.controller';

export class HeaderComponent {
    constructor() {
        this.bindings = {
        };
        this.controller = HeaderController;
        this.controllerAs = 'vm';
        this.templateUrl = './dist/app/layout/header/header.partial.html';
    }
}