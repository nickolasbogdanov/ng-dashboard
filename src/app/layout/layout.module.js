import angular from 'angular';
import {HeaderComponent} from './header/header.component';

const components = angular.module('app.layout', []);

components.component('dashboardHeader', new HeaderComponent());
