import {GraphBarController} from './bar.controller';

export class GraphBarComponent {
    constructor() {
        this.bindings = {
        };
        this.controller = GraphBarController;
        this.controllerAs = 'vm';
    }
}