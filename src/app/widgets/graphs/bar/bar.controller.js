import c3 from 'c3';
import {barModel} from './bar.model';

export class GraphBarController {
    // @ngInject
    constructor($element) {
        this.$element = $element;
    }
    
    $postLink(){
        barModel.bindto = this.$element[0];
        c3.generate(barModel);
    }
}