export const barModel = {
    data: {
        columns: [
            ['Opened', 30, 100, 200],
            ['In Progress', 50, 200, 300],
            ['On Hold', 30, 200, 200, 400, 150, 250],
            ['Archived', 230, 200, 200, 300, 250, 250]
        ],
        type: 'bar',
        groups: [
            ['Opened', 'In Progress', 'On Hold', 'Archived']
        ]
    },
    grid: {
        y: {
            lines: [{ value: 0 }]
        }
    }
};