import {GraphLineController} from './line.controller';

export class GraphLineComponent {
    constructor() {
        this.bindings = {
        };
        this.controller = GraphLineController;
        this.controllerAs = 'vm';
    }
}