import c3 from 'c3';
import {lineModel} from './line.model';

export class GraphLineController {
    // @ngInject
    constructor($element) {
        this.$element = $element;
    }

    $postLink() {
        lineModel.bindto = this.$element[0];
        c3.generate(lineModel);
    }
}