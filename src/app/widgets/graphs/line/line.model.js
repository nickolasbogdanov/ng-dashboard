export const lineModel = {
    data: {
        columns: [
            ['Responsive', 30, 200, 100, 400, 150, 250],
            ['Non-Responsive', 50, 20, 10, 40, 15, 25],
            ['Privileged', 70, 80, 80, 300, 400, 500],
            ['None-Privileged', 10, 20, 30, 40, 50, 60]
        ]
    }
};