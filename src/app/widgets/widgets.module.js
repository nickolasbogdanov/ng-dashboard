import angular from 'angular';
import {GraphBarComponent} from '../widgets/graphs/bar/bar.component';
import {GraphLineComponent} from '../widgets/graphs/line/line.component';
import 'npm:c3@0.4.11/c3.min.css!';

const widgets = angular.module('app.widgets', []);

widgets.component('graphBar', new GraphBarComponent());
widgets.component('graphLine', new GraphLineComponent());