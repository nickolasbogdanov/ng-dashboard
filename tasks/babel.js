module.exports = (gulp, scripts, dist) => {
    return () => {
        const babel = require('gulp-babel');
        const sourcemaps = require('gulp-sourcemaps');
        const ngAnnotate = require('gulp-ng-annotate');

        return gulp.src(scripts)
            .pipe(sourcemaps.init())
            .pipe(babel())
            .pipe(ngAnnotate())
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(dist));
    }

};