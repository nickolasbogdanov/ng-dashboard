const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const eslint = require('gulp-eslint');
const gulp = require('gulp');

module.exports = function () {
    return gulp.src('src/**/*.js')
        .pipe(eslint())
        .pipe(plumber())
        .pipe(eslint.failOnError())
        .on('error', notify.onError(function (err) {
            return {
                title: "Lint Error",
                message: err.message
            };
        }));

};
