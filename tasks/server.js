module.exports = (serverRootDir, openBrowser) => {
    return () => {

        var onOffFlag = '✗';
        const browserSync = require('browser-sync').init({
            server: {
                baseDir: [serverRootDir]
            },
            open: openBrowser,
            host: 'localhost',
            browser: 'default',
            notify: false
        });

        console.info(`\n\tWatching JavaScript ${onOffFlag}\n`);

        return browserSync;
    };
};