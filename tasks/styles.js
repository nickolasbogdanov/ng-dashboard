module.exports = (gulp, styles, destination) => () => {
    const less = require('gulp-less');
    const combiner = require('stream-combiner2').obj;
    const sourcemaps = require('gulp-sourcemaps');

    return combiner(gulp.src(styles),
        sourcemaps.init(),
        less(),
        sourcemaps.write(),
        gulp.dest(destination)
    );
};
