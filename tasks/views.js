module.exports = (gulp, srcDir, destination) => () => {

    gulp.src(srcDir).pipe(gulp.dest(destination));
};
